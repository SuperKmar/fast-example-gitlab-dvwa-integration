# README

This is an example of Wallarm FAST running security tests in the Gitlab CI/CD pipeline. The target application is a DVWA.

All build results: https://gitlab.com/wallarm/fast-example-gitlab-dvwa-integration/pipelines

## How to reproduce example

1. Create your FAST node and get `WALLARM_API_TOKEN` here https://us1.my.wallarm.com/nodes
2. Fork this repository
3. Add project into Circle CI (first build will fail without `WALLARM_API_TOKEN`)
4. Add env-variable `WALLARM_API_TOKEN` in the project settings 
5. Rerun build. It will find some vulnerabilities

## Useful links

- More about Wallarm FAST: https://wallarm.com/products/fast
- More about Gitlab: https://about.gitlab.com/company/
- DVWA application's source code: https://github.com/wallarm/fast-example-dvwa
- Full FAST documentation: https://docs.fast.wallarm.com

TRY WALLARM FAST NOW: https://fast.wallarm.com/signup
